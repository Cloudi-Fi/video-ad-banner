Usage instructions

After unzipping this package, please check if the following files are present AND in the same directory:
- ad.mp4
- index.html
- README.txt

If you need to make a new banner from this one:
- first, COPY the entire directory into a new one and give it an adequate name
- then, REPLACE "ad.mp4" with your own video file but please keep the same name or it won't work
- if your video has no audio, please ADD the "muted" attribute to the <video> tag inside index.html (i.e <video muted .../>)
- finally, ZIP your new directory and send it to the banner form

Regarding the video file itself:
- it MUST be MP4/H.264 in order to support the widest range of browsers
- try to keep it as light as possible for maximum reach
- no need for 1080p or 720p on smartphones, 640x360 is good enough.